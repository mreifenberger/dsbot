var queue = [];

setInterval(function()
{
    if (performance.navigation.type == 1 &&$('#building_wrapper') == undefined) {
        update();
    }
    if(queue.length > 0)
    {
        if(canBuild(queue[0]))
        {
            BuildingMain.build(queue[0]);
            queue.shift();
        }
    }
    update();
},1000)

function canBuild(id)
{
    var wood = parseInt($('#wood').text());
    var stone = parseInt($('#stone').text());
    var iron = parseInt($('#iron').text());

    var wood_needed = BuildingMain.buildings[id].wood;
    var stone_needed = BuildingMain.buildings[id].stone;
    var iron_needed = BuildingMain.buildings[id].iron;

    var resourcesCheck = wood >= wood_needed && stone >= stone_needed && iron >= iron_needed;

    return (resourcesCheck && BuildingMain.order_count <= 1)
}

function tryBuild(id)
{
    update();
    queue.push(id);
    console.log(queue);
}


function init()
{
    $('#building_wrapper').prepend('<div id="bot_queue"></div>');
    update();
}

function update()
{
    var buildings = Object.getOwnPropertyNames(BuildingMain.buildings);
    var html = '';

    html += '<table style="width: 100%" class="vis nowrap">';
    html += '<th width="20%">Gebäude</th>';
    html += '<th colspan="10" width="70%">Warteschlange</th>';
    html += '<th width="10%">Bauen</th>';


    for(var i=0; i<buildings.length; i++)
    {
        html += '<tr>';
        html += '<td><img src="https://dsde.innogamescdn.com/8.34.5/26689/graphic/buildings/mid/' + buildings[i] + '1.png">';
        html += '<a href="#">' + BuildingMain.buildings[buildings[i]].name + '</a></td>';

        for(var j=0; j<10; j++)
        {
            if(buildings[i] == queue[j])
            {
                html += '<td style="background-color: green"><button onclick="cancelBuilding(' + "'" + j + "'" +')">Abbrechen</button></td>';
            }
            else
            {
                html += '<td></td>';
            }
        }

        html += '<td><button class="btn" onclick="tryBuild(' + "'" + buildings[i] + "'" + ')">bauen</button></td>';
        html += '</tr>';
    }

    html += '</table><br>';

    if($('#bot_queue').length == 0)
    {
        init();
    }

    $('#bot_queue').html(html);
}

function cancelBuilding(index)
{
    /*
    BuildingMain.cancel(id);
    var acceptButton = $('html.js.flexbox.history.draganddrop.borderimage.textshadow.cssanimations.localstorage.sessionstorage.filereader.json.performance body#ds_body.desktop div#fader div#confirmation-box.confirmation-box div.confirmation-box-content-pane div.confirmation-box-content div.confirmation-buttons button.btn.evt-confirm-btn.btn-confirm-yes');
    acceptButton.click(); */

    console.log(queue);
    queue.splice(index,1);
    update();
}

init();

