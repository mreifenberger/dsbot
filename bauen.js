
var queue = [];

setInterval(function()
{
	if(queue.length > 0)
	{
		if(canBuild(queue[0]))
		{
			BuildingMain.build(queue[0]);
			queue.shift();
		}
	}
},5000)

function canBuild(id)
{
	var wood = parseInt($('#wood').text());
	var stone = parseInt($('#stone').text());
	var iron = parseInt($('#iron').text());

	var wood_needed = BuildingMain.buildings[id].wood;
	var stone_needed = BuildingMain.buildings[id].stone;
	var iron_needed = BuildingMain.buildings[id].iron;

	var resourcesCheck = wood >= wood_needed && stone >= stone_needed && iron >= stone_needed;

	return (resourcesCheck && BuildingMain.order_count <= 1)
}


function tryBuild(id)
{
	queue.push(id);
}